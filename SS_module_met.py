#Emma Buchanan 17/06/2021

#To run: python3 SS_module_met.py smartscope_filename.txt 

import numpy as np
import matplotlib.pyplot as plt
import sys
import seaborn as sns
import configparser
from datetime import datetime, timezone

filename = sys.argv[1]

hybrid_flex_thickness = 0.38
powerboard_thickness = 0.400

xref = [ 63.873, 63.873, 48.881,48.881,33.753,33.753]
yref = [97.118, 0.788, 93.201, 24.951, 97.118,0.812 ]



fid_x = []
fid_y = []
fid_z = []

ABCX_x = []
ABCX_y = []
ABCX_z = []

HX0_x = []
HX0_y = []
HX0_z = []

HX1_x = []
HX1_y = []
HX1_z = []

HCCX_x = []
HCCX_y = []
HCCX_z = []

PB_1_x = []
PB_1_y = []
PB_1_z = []

PB_3_x = []
PB_3_y = []
PB_3_z = []

PB_4_x = []
PB_4_y = []
PB_4_z = []

SB_x = []
SB_y = []
SB_z = []

C_x = []
C_y = []
C_z = []

ABCY_x = []
ABCY_y = []
ABCY_z = []

HY0_x = []
HY0_y = []
HY0_z = []

HY1_x = []
HY1_y = []
HY1_z = []

HCCY_x = []
HCCY_y = []
HCCY_z = []


Hybrid_X_DZ =list()
Powerboard_DZ =list()
Powerboard_CS_DZ =list()
Hybrid_Y_DZ =list()

count = 0

with open(filename) as fn:
    print("opening", filename)
    for line in fn:
        a = line.split()

        ##Fiducials
        #hybridX
        name = "hybrid_fid"
        if "X {}".format(name) in line:
            fid_x.append(float(a[3]))
        if "Y {}".format(name) in line:
            fid_y.append(float(a[3]))
        if "Z {}".format(name) in line:
            fid_z.append(float(a[3]))
            
        #PB
        
        name = "powerboad_"
        if "X {}".format(name) in line:
            fid_x.append(float(a[3]))
        if "Y {}".format(name) in line:
            fid_y.append(float(a[3]))
        if "Z {}".format(name) in line:
            fid_z.append(float(a[3])) 
                
        #hybridY
        name = "hybridY_fi"
        if "X {}".format(name) in line:
            fid_x.append(float(a[3]))
        if "Y {}".format(name) in line:
            fid_y.append(float(a[3]))
        if "Z {}".format(name) in line:
            fid_z.append(float(a[3]))

        ##ABCX positions
        for l in range(0,10):
            if l==0:
                for j in range(0,6):
                    hybridz_name = "ABCX{}_p{}.Z".format(l, j)
                    if hybridz_name in line:
                        ABCX_z.append(float(a[3]))
                    hybridy_name = "ABCX{}_p{}.Y".format(l, j)
                    if hybridy_name in line:
                        ABCX_y.append(float(a[3]))
                    hybridx_name = "ABCX{}_p{}.X".format(l, j)
                    if hybridx_name in line:
                        ABCX_x.append(float(a[3]))               
            else:
                for j in range(0, 3):        
                    hybridz_name = "ABCX{}_p{}.Z".format(l, j)
                    if hybridz_name in line:
                        ABCX_z.append(float(a[3]))
                    hybridy_name = "ABCX{}_p{}.Y".format(l, j)
                    if hybridy_name in line:
                        ABCX_y.append(float(a[3]))
                    hybridx_name = "ABCX{}_p{}.X".format(l, j)
                    if hybridx_name in line:
                        ABCX_x.append(float(a[3]))
        
        ##HCCX positions
        for l in range(0, 3):
            name = 'HCC_X_0_p{}'.format(l)

            if 'Z '+name in line:
                HCCX_z.append(float(a[3]))

            if 'Y '+name in line:
                HCCX_y.append(float(a[3]))

            if 'X '+name in line:
                HCCX_x.append(float(a[3]))
        
        ##HX0 positions
        for l in range(0, 3):
            name = 'H_X_0_p{}'.format(l)
            if 'X '+name+'.X' in line:
                HX0_x.append(float(a[3]))
            if 'Y '+name+'.Y' in line:
                HX0_y.append(float(a[3]))
            if 'Z '+name+'.Z' in line:
                HX0_z.append(float(a[3]))
                
        
        ##HX1 positions
        for l in range(0, 4):
            name = 'H_X_1_p{}'.format(l)
            if 'X '+name+'.X' in line:
                HX1_x.append(float(a[3]))
            if 'Y '+name+'.Y' in line:
                HX1_y.append(float(a[3]))
            if 'Z '+name+'.Z' in line:
                HX1_z.append(float(a[3]))
        
        ##HX2 positions
        name = 'H_X_2'
        if name+'.X' in line:
            HX2_x = float(a[3])
        if name+'.Y' in line:
            HX2_y = float(a[3])
        if name+'.Z' in line:
            HX2_z = float(a[3])
        
        ##Powerboard positions
        #PB1
        for l in range(0, 4):
            name = "PB_1_p{}".format(l)
            if name+'.X' in line:
                PB_1_x.append(float(a[3]))
            if name+'.Y' in line:
                PB_1_y.append(float(a[3]))
            if name+'.Z' in line:
                PB_1_z.append(float(a[3]))
        #PB2
        if "PB_2.X" in line:
            PB_2_x = float(a[3])
        if "PB_2.Y" in line:
            PB_2_y = float(a[3])
        if "PB_2.Z" in line:
            PB_2_z = float(a[3])
        #PB3
        for l in range(0, 4):
            name = "PB_3_p{}".format(l)
            if name+'.X' in line:
                PB_3_x.append(float(a[3]))
            if name+'.Y' in line:
                PB_3_y.append(float(a[3]))
            if name+'.Z' in line:
                PB_3_z.append(float(a[3]))
        #PB4
        for l in range(0, 4):
            name = "PB_4_p{}".format(l)
            if name+'.X' in line:
                PB_4_x.append(float(a[3]))
            if name+'.Y' in line:
                PB_4_y.append(float(a[3]))
            if name+'.Z' in line:
                PB_4_z.append(float(a[3]))
        #PB5
        if "PB_5.X" in line:
            PB_5_x = float(a[3])
        if "PB_5.Y" in line:
            PB_5_y = float(a[3])
        if "PB_5.Z" in line:
            PB_5_z = float(a[3])
        
        ##SB position
        for l in range(0, 5):
            name = "SB_p{}".format(l)
            if name+'.X' in line:
                SB_x.append(float(a[3]))
            if name+'.Y' in line:
                SB_y.append(float(a[3]))
            if name+'.Z' in line:
                SB_z.append(float(a[3]))

        ##Capacitor Positions
        for l in range(1, 5):
            name = "C{}".format(l)
            if name+'.X' in line:
                C_x.append(float(a[3]))
            if name+'.Y' in line:
                C_y.append(float(a[3]))
            if name+'.Z' in line:
                C_z.append(float(a[3]))
        
        ##ABCY positions
        for l in range(0,10):
            if l==0:
                for j in range(0,6):
                    hybridz_name = "ABCY{}_p{}.Z".format(l, j)
                    if hybridz_name in line:
                        ABCY_z.append(float(a[3]))
                    hybridy_name = "ABCY{}_p{}.Y".format(l, j)
                    if hybridy_name in line:
                        ABCY_y.append(float(a[3]))
                    hybridx_name = "ABCY{}_p{}.X".format(l, j)
                    if hybridx_name in line:
                        ABCY_x.append(float(a[3]))               
            else:
                for j in range(0, 3):        
                    hybridz_name = "ABCY{}_p{}.Z".format(l, j)
                    if hybridz_name in line:
                        ABCY_z.append(float(a[3]))
                    hybridy_name = "ABCY{}_p{}.Y".format(l, j)
                    if hybridy_name in line:
                        ABCY_y.append(float(a[3]))
                    hybridx_name = "ABCY{}_p{}.X".format(l, j)
                    if hybridx_name in line:
                        ABCY_x.append(float(a[3]))
        
        ##HCCY positions
        for l in range(0, 3):
            name = 'HCC_Y_0_p{}'.format(l)

            if 'Z '+name in line:
                HCCY_z.append(float(a[3]))

            if 'Y '+name in line:
                HCCY_y.append(float(a[3]))

            if 'X '+name in line:
                HCCY_x.append(float(a[3]))
        
        ##HY0 positions
        for l in range(0, 3):
            name = 'H_Y_0_p{}'.format(l)
            if 'X '+name+'.X' in line:
                HY0_x.append(float(a[3]))
            if 'Y '+name+'.Y' in line:
                HY0_y.append(float(a[3]))
            if 'Z '+name+'.Z' in line:
                HY0_z.append(float(a[3]))
                
        
        ##HY1 positions
        for l in range(0, 4):
            name = 'H_Y_1_p{}'.format(l)
            if 'X '+name+'.X' in line:
                HY1_x.append(float(a[3]))
            if 'Y '+name+'.Y' in line:
                HY1_y.append(float(a[3]))
            if 'Z '+name+'.Z' in line:
                HY1_z.append(float(a[3]))
        
        ##HY2 positions
        name = 'H_Y_2'
        if name+'.X' in line:
            HY2_x = float(a[3])
        if name+'.Y' in line:
            HY2_y = float(a[3])
        if name+'.Z' in line:
            HY2_z = float(a[3])
        



##ABCX
#average ABCX height 
ABCX_z_av = list()            
ABCX_z_array = np.reshape(ABCX_z, (11,3))
ABCX_z_av_array = np.average(ABCX_z_array, axis =1)
for l in range(0,10):
    ABCX_z_av.append(((ABCX_z_av_array[l] + ABCX_z_av_array[l+1])/2))
asic_glue_height = list(1000*np.subtract(ABCX_z_av, hybrid_flex_thickness))
#average ABCX X&Y
ABCX_x_av = list()            
ABCX_x_array = np.reshape(ABCX_x, (11,3))
ABCX_x_av_array = np.average(ABCX_x_array, axis =1)
for l in range(0,10):
    ABCX_x_av.append(((ABCX_x_av_array[l] + ABCX_x_av_array[l+1])/2))

ABCX_y_av = list()            
ABCX_y_array = np.reshape(ABCX_y, (11,3))
ABCX_y_av_array = np.average(ABCX_y_array, axis =1)
for l in range(0,10):
    ABCX_y_av.append(((ABCX_y_av_array[l] + ABCX_y_av_array[l+1])/2))

##HCCX average 
HCCX_av = np.average(HCCX_z)
asic_glue_height.append(1000*(HCCX_av - hybrid_flex_thickness))

##H_X_0 average 
HX0_av = np.average(HX0_z)
asic_glue_height.append(1000*(HX0_av - hybrid_flex_thickness))

##H_X_1 average 
HX1_av = np.average(HX1_z)
asic_glue_height.append(1000*(HX1_av - hybrid_flex_thickness))

##H_X_2 
asic_glue_height.append(1000*(HX2_z - hybrid_flex_thickness))


##PB averages
PB_glue_height = []
#PB1
pb1_av = np.average(PB_1_z)
PB_glue_height.append(1000*(pb1_av - powerboard_thickness))
pb1_av_x = np.average(PB_1_x)
pb1_av_y = np.average(PB_1_y)
#PB2
PB_glue_height.append(1000*(PB_2_z - powerboard_thickness))
#PB3
pb3_av = np.average(PB_3_z)
PB_glue_height.append(1000*(pb3_av - powerboard_thickness))
pb3_av_x = np.average(PB_3_x)
pb3_av_y = np.average(PB_3_y)
#PB4
pb4_av = np.average(PB_4_z)
PB_glue_height.append(1000*(pb4_av - powerboard_thickness))
pb4_av_x = np.average(PB_4_x)
pb4_av_y = np.average(PB_4_y)
#PB5
PB_glue_height.append(1000*(PB_5_z - powerboard_thickness))

##C and SB
C_SB_height = C_z 
#average SB
SB_z_av = np.average(SB_z)
C_SB_height.append(SB_z_av)
C_SB_height = np.multiply(C_SB_height, 1000)

##Fiducials 
fid_rel_x = 1000*np.subtract(fid_x, xref)
fid_rel_y = 1000*np.subtract(fid_y, yref)

##ABCY
#average ABCY height 
ABCY_z_av = list()            
ABCY_z_array = np.reshape(ABCY_z, (11,3))
ABCY_z_av_array = np.average(ABCY_z_array, axis =1)
for l in range(0,10):
    ABCY_z_av.append(((ABCY_z_av_array[l] + ABCY_z_av_array[l+1])/2))
asicY_glue_height = list(1000*np.subtract(ABCY_z_av, hybrid_flex_thickness))
asicY_glue_height.reverse()
#average ABCY X&Y
ABCY_x_av = list()            
ABCY_x_array = np.reshape(ABCY_x, (11,3))
ABCY_x_av_array = np.average(ABCY_x_array, axis =1)
for l in range(0,10):
    ABCY_x_av.append(((ABCY_x_av_array[l] + ABCY_x_av_array[l+1])/2))

ABCY_y_av = list()            
ABCY_y_array = np.reshape(ABCY_y, (11,3))
ABCY_y_av_array = np.average(ABCY_y_array, axis =1)
for l in range(0,10):
    ABCY_y_av.append(((ABCY_y_av_array[l] + ABCY_y_av_array[l+1])/2))

##HCCY average 
HCCY_av = np.average(HCCY_z)
asicY_glue_height.append(1000*(HCCY_av - hybrid_flex_thickness))

##H_Y_0 average 
HY0_av = np.average(HY0_z)
asicY_glue_height.append(1000*(HY0_av - hybrid_flex_thickness))

##H_Y_1 average 
HY1_av = np.average(HY1_z)
asicY_glue_height.append(1000*(HY1_av - hybrid_flex_thickness))

##H_X_2 
asicY_glue_height.append(1000*(HY2_z - hybrid_flex_thickness))
        
##########
#Multiplot -  only mean heights 
###########
plt.rcParams.update({'axes.labelsize': 6, 'axes.titlesize':10, 'xtick.labelsize':8, 'ytick.labelsize':8})
fig = plt.figure(figsize = (6, 8))
axes = fig.subplots(ncols=2, nrows = 3)

#Fiducials
ax_Fidx = sns.scatterplot(x = np.arange(0,len(fid_rel_x),1), y= fid_rel_x, marker = 'D', ax = axes[0][0])
ax_Fidx.set(ylabel = 'difference from reference X', title = 'Fiducial X Coordinate', xlabel = 'measurement number')

ax_Fidy = sns.scatterplot(x = np.arange(0,len(fid_rel_y),1), y= fid_rel_y, marker = 'D', ax = axes[0][1])
ax_Fidy.set(ylabel = 'difference from reference Y', title = 'Fiducial Y Coordinate', xlabel = 'measurement number')


#HybridX Glue Height
hybrid_spec_min = 80 
hybrid_spec_max = 160

background = 'white'
if min(asic_glue_height[0:10]) < hybrid_spec_min: 
    background = 'lightcoral'
if min(asic_glue_height) < hybrid_spec_min: 
    ymin = min(asic_glue_height) - 20
else:
    ymin = hybrid_spec_min- 20                                                                                                                                                                             

if max(asic_glue_height) > hybrid_spec_max:
    ymax = max(asic_glue_height) + 20
    background = 'lightcoral'
else:
    ymax = hybrid_spec_max + 20

axis_labels = ["ABCX0", "ABCX1", "ABCX2", "ABCX3", "ABCX4", "ABCX5", "ABCX6", "ABCX7", "ABCX8", "ABCX9", "HCCX", "HX0", "HX1", "HX2"]

HybridX_DZ=sns.scatterplot(x=axis_labels, y = asic_glue_height,   marker='D', color='#8829F0', ax=axes[1][0])
HybridX_DZ.set(ylabel='Glue Thickness [\u03BCm]',title = 'Hybrid X',  ylim=(ymin,ymax), facecolor = background)
HybridX_DZ.axes.axhline(y = hybrid_spec_min, xmin = 0, xmax = 0.7,color='black', linewidth=1, linestyle='--')
HybridX_DZ.axes.axhline(y = hybrid_spec_max, color='black', linewidth=1, linestyle='--')
HybridX_DZ.set_xticklabels(axis_labels, rotation = 45)

#HybridY Glue Height
hybrid_spec_min = 80 
hybrid_spec_max = 160

background = 'white'
if min(asicY_glue_height[0:10]) < hybrid_spec_min: 
    background = 'lightcoral'
if min(asicY_glue_height) < hybrid_spec_min: 
    ymin = min(asicY_glue_height) - 20
else:
    ymin = hybrid_spec_min- 20                                                                                                                                                                             

if max(asicY_glue_height) > hybrid_spec_max:
    ymax = max(asicY_glue_height) + 20
    background = 'lightcoral'
else:
    ymax = hybrid_spec_max + 20

axis_labels = ["ABCX9", "ABCX8", "ABCX7", "ABCX6", "ABCX5", "ABCX4", "ABCX3", "ABCX2", "ABCX1", "ABCX0", "HCCX", "HX0", "HX1", "HX2"]

HybridX_DZ=sns.scatterplot(x=axis_labels, y=asicY_glue_height,   marker='D', color='#8829F0', ax=axes[1][1])
HybridX_DZ.set(ylabel='Glue Thickness [\u03BCm]',title = 'Hybrid Y',  ylim=(ymin,ymax), facecolor = background)
HybridX_DZ.axes.axhline(y = hybrid_spec_min, xmin = 0, xmax = 0.7,color='black', linewidth=1, linestyle='--')
HybridX_DZ.axes.axhline(y = hybrid_spec_max, color='black', linewidth=1, linestyle='--')
HybridX_DZ.set_xticklabels(axis_labels, rotation = 45)


#PowerBoard Glue Height

background = 'white'
if min(PB_glue_height) < hybrid_spec_min:
    ymin = min(PB_glue_height) - 20
    background = 'lightcoral'
else:
    ymin = hybrid_spec_min- 20

if max(PB_glue_height) > hybrid_spec_max:
    ymax = max(PB_glue_height) + 20
    background = 'lightcoral'
else:
    ymax = hybrid_spec_max + 20

axis_labels = ["PB1", "PB2", "PB3", "PB4", "PB5"]
Pb_DZ=sns.scatterplot(x=axis_labels, y=PB_glue_height,   marker='D', color='#8829F0', ax=axes[2][0])
Pb_DZ.set(ylabel='Glue Thickness [\u03BCm]', ylim=(ymin,ymax), title = 'Powerboard', facecolor = background)
Pb_DZ.axes.axhline(y = hybrid_spec_min, color='black', linewidth=1, linestyle='--')
Pb_DZ.axes.axhline(y = hybrid_spec_max, color='black', linewidth=1, linestyle='--')
Pb_DZ.set_xticklabels(axis_labels, rotation = 45)


#Capacitors and switchboards
CS_z=sns.scatterplot(x=axis_labels, y=C_SB_height,   marker='D', color='#8829F0', ax = axes[2][1])
CS_z.set(ylabel='Height from sensor surface [mm]', title = 'Cap/SB Mean')
plt.xticks(rotation=45)

plt.tight_layout()
plt.show(block = False)


plot_plotly = input('Plot interactive graphs? (y/n)')

if plot_plotly == 'y':

###############
##Interactive Plots
##############

    import plotly.graph_objects as go
    from plotly.subplots import make_subplots

    fig = make_subplots(
        rows=3, cols=2, subplot_titles=('Fiducial X positions', 'Fiducial Y positions','HybridX', 'HybridY', 'Powerboard', 'Capacitors and Switchboard'), horizontal_spacing = 0.2)

    fig.add_trace(
        go.Scatter(x = np.arange(0, len(fid_x_rel), 1), 
                    y=fid_x_rel, 
                    mode = 'markers', 
                    hoverinfo = 'x+y', 
                    showlegend = False,
                    marker = dict(color = 'blue')), 
                    row = 1, col =1)
    
    fig.update_yaxes(title_text = 'difference from reference X', row = 1, col = 1)
    fig.update_xaxes(title_text = 'Measurement Number', row = 1, col = 1)

    fig.add_trace(
        go.Scatter(x=np.arange(0,len(fid_y_rel),1), 
                    y= fid_y_rel, 
                    mode = 'markers',
                    hoverinfo = 'x+y',
                    showlegend = False,
                    marker = dict(color = 'pink')),
                    row = 1, col =2
    )
    fig.update_yaxes(title_text = 'difference from reference Y', row = 1, col = 2)
    fig.update_xaxes(title_text = 'Measurement Number', row = 1, col = 2)

    axis_labels = ["ABCX0", "ABCX1", "ABCX2", "ABCX3", "ABCX4", "ABCX5", "ABCX6", "ABCX7", "ABCX8", "ABCX9", "HCCX", "HX0", "HX1", "HX2"]

    fig.add_trace(
        go.Scatter(x=axis_labels,
                    y=asic_glue_height,
                    mode="markers",
                    hoverinfo="text",
                    showlegend = False,
                    marker=dict(color='purple')),
        row=2, col=1
    )
    fig.update_yaxes(title_text = 'Glue Thickness [\u03BCm]', row = 2, col = 1)

    fig.add_trace(
        go.Scatter(x=axis_labels,
                    y=asicY_glue_height,
                    mode="markers",
                    hoverinfo="text",
                    showlegend = False,
                    marker=dict(color='purple')),
        row=2, col=2
    )

    fig.update_yaxes(title_text = 'Glue Thickness [\u03BCm]', row = 2, col = 2)

    axis_labels_2 = ["PB1", "PB2", "PB3", "PB4", "PB5"]
    fig.add_trace(
        go.Scatter(x=axis_labels_2,
                    y=PB_glue_height,
                    mode="markers",
                    hoverinfo="x+y",
                    showlegend = False,
                    marker=dict(color='green')),
        row=3, col=1
    )

    fig.update_yaxes(title_text = 'Glue Thickness [\u03BCm]', row = 3, col = 1)
    

    axis_labels_3 =  ["C1", "C2", "C3", "C4", "SB"]
    fig.add_trace(
        go.Scatter(x=axis_labels_3,
                    y=C_SB_height,
                    mode="markers",
                    showlegend = False,
                    hoverinfo="x+y",
                    marker=dict(color='red')),
        row=3, col=2
    )

    fig.update_yaxes(title_text = 'Height from sensor surface', row = 3, col = 2)

    fig.add_hline(y = 80, line_dash = 'dash', line_color = 'black', row = 2, col = 'all')
    fig.add_hline(y = 160, line_dash = 'dash', line_color = 'black', row = 2, col = 'all')

    fig.add_hline(y = 80, line_dash = 'dash', line_color = 'black', row = 3, col = 1)
    fig.add_hline(y = 160, line_dash = 'dash', line_color = 'black', row = 3, col = 1)



    fig.update_layout(width =1000, height =  1500)
    fig.show()


#############
## Database output files
#############

#set config file
config_file = 'database_module_metrology.config'
parser = configparser.ConfigParser()
parser.read(config_file)

##Output to file1
#output file for the database                                                 
filename2 =parser.get('output_file_name','name')
file1 = open(filename2,"w")


date = parser.get('header', 'meas_date')
time = parser.get('header', 'meas_time')
header = """#---Header"""+"""                                                      
EC or Barrel:\t\t\t\t"""+parser.get('header','ec_or_barrel')+"""     
Module Type:\t\t\t\t"""+parser.get('header','module_type')+"""  
Module Ref Number:\t\t\t"""+parser.get('header','module_ref')+"""   
Date:\t\t\t\t\t"""+datetime.strptime(date + " " + time, "%Y-%m-%d %H:%M:%S").astimezone().isoformat()+""" 
Institute:\t\t\t\t"""+parser.get('header','institute')+"""    
Operator:\t\t\t\t"""+parser.get('header','operator')+"""      
Instrument type:\t\t\t"""+parser.get('header','instrument')+"""      
Run Number:\t\t\t\t\t"""+parser.get('header','test_run_num')+"""   
Measurement program version:\t"""+parser.get('header','meas_program')
file1.write(header)

positions_title = "\n#---Positions:"
file1.write(positions_title)
positions_head = "\n#Location\t\t\t\tX [mm]\t\t\t\tY [mm]"
file1.write(positions_head)

for i in range(1,3):
    string = "\nH_X_P{}\t\t\t\t\t{}\t\t\t\t{}".format(i, round(0.001*fid_rel_x[i-1],3), round(fid_rel_y[i-1],3))
    file1.write(string)
for i in range(1,3):
    string = "\nPB_P{}\t\t\t\t\t{}\t\t\t\t{}".format(i, round(0.001*fid_rel_x[i+1],3), round(fid_rel_y[i+1],3))
    file1.write(string)

glue_title = "\n#---Glue Heights:"
file1.write(glue_title)
glue_head = "\n#Location\t\t\tType\t\t\tX [mm]\t\t\tY [mm]\t\t\tZ [mm]"
file1.write(glue_head)

##Glue heights
#ABCX
for i in range(0, 10):
    for j in range(0, 3):
        string = "\nABC_X_{}\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(i, 0, round(ABCX_x[3*i+j],3), round(ABCX_y[3*i+j],3), round(ABCX_z[3*i+j],3))
        file1.write(string)
#HCCX
for i in range(0, 3):
    string2 = "\nHCC_X_0\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(0, round(HCCX_x[i],3), round(HCCX_y[i],3), round(HCCX_z[i],3))
    file1.write(string2)
#HX0
for i in range(0,2):
    string = "\nH_X_0\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(0, round(HX0_x[i],3), round(HX0_y[i],3), round(HX0_z[i],3))
#HX1
for i in range(0,4):
    string = "\nH_X_1\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(0,round(HX1_x[i],3), round(HX1_y[i],3), round(HX1_z[i],3))
#HX2
string = "\nH_X_2\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(0, round(HX2_x, 3), round(HX2_y, 3), round(HX2_z, 3))
#PB1
string = "\nPB_1\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(0, round(pb1_av_x,3),round(pb1_av_y,3),round(pb1_av,3))
file1.write(string)
#PB2
string = "\nPB_2\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(0, round(PB_2_x,3),round(PB_2_y,3),round(PB_2_z,3))
file1.write(string)
#PB3
string = "\nPB_3\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(0, round(pb3_av_x,3),round(pb3_av_y,3),round(pb3_av,3))
file1.write(string)
#PB4
string = "\nPB_4\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(0, round(pb4_av_x,3),round(pb4_av_y,3),round(pb4_av,3))
file1.write(string)
#PB5
string = "\nPB_5\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(0, round(PB_5_x,3),round(PB_5_y,3),round(PB_5_z,3))
file1.write(string)
#ABCY
for i in range(0, 10):
    for j in range(0, 3):
        string = "\nABC_Y_{}\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(i, 0, round(ABCY_x[3*i+j],3), round(ABCY_y[3*i+j],3), round(ABCY_z[3*i+j],3))
        file1.write(string)

total_stack_title = "\n#---Other heights:"
total_stack_head = "\n#Location\t\t\tType\t\t\tX [mm]\t\t\tY [mm]\t\t\tZ [mm]"
file1.write(total_stack_title)
file1.write(total_stack_head)

for i in range(1,5):
    string = "\nC{}\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(i, 0, round(C_x[i-1], 3), round(C_y[i-1], 3), round(C_z[i-1], 3))
    file1.write(string)
for i in range(0, 5):
    string2 = "\nShield\t\t\t\t{}\t\t\t{}\t\t\t{}\t\t\t{}".format(0, round(SB_x[i],3), round(SB_y[i],3), round(SB_z[i],3))
    file1.write(string2)
file1.close()


##############
##json

filename1 =parser.get('output_file_name2','name')
file1 = open(filename1,"w")

date = parser.get('header2', 'meas_date')
time= parser.get('header2', 'meas_time')

data = {}
data["component"] = parser.get('header2', 'component')
data["date"] = datetime.strptime(date + " " + time, "%Y-%m-%d %H:%M:%S").astimezone().isoformat()
data["runNumber"] = parser.get('header2', 'test_run_num')
data["institution"] = parser.get('header2', 'institute')
data["passed"] = parser.get('header2', 'passed')
data["problems"] = parser.get('header2', 'problems')

properties = {}
properties["MACHINE"] = parser.get('header2', 'instrument')
properties["OPERATOR"] = parser.get('header2', 'operator')
data["properties"] = properties

results = {}

hybrid_position = {}
for i in range(1,3):
    hybrid_position["H_X_P{}".format(i)] = [round(fid_rel_x[i-1],3), round(fid_rel_y[i-1],3)]
for i in range(1,3):
    hybrid_position["H_Y_P{}".format(i)] = [round(fid_rel_x[i+3],3), round(fid_rel_y[i+1],3)]
results["HYBRID_POSITION"] = hybrid_position

PB_position = {}
for i in range(1,3):
    PB_position["PB_P{}".format(i)] = [round(fid_rel_x[i+1],3), round(fid_rel_y[i+1],3)]
results["PB_POSITION"] = PB_position

hybrid_glue_thickness={}
for i in range(0,10):
    hybrid_glue_thickness["ABC_X_{}".format(i)] = round(asic_glue_height[i],3)
hybrid_glue_thickness["HCC_X_0"] = round(asic_glue_height[10],3)
for i in range(0, 3):
    hybrid_glue_thickness["H_X_{}".format(i)] = round(asic_glue_height[11 + i],3)
for i in range(0,10):
    hybrid_glue_thickness["ABC_Y_{}".format(i)] = round(asicY_glue_height[i],3)
hybrid_glue_thickness["HCC_Y_0"] = round(asicY_glue_height[10],3)
for i in range(0, 3):
    hybrid_glue_thickness["H_Y_{}".format(i)] = round(asicY_glue_height[11 + i],3)
results["HYBRID_GLUE_THICKNESS"] = hybrid_glue_thickness

PB_glue_thickness = {}
for i in range(1,5):
    PB_glue_thickness["PB_{}".format(i)] = round(PB_glue_height[i-1],3)
results["PB_GLUE_THICKNESS"] = PB_glue_thickness

cap_height = {}
for i in range(1,5):
    cap_height["C{}".format(i)] = round(C_SB_height[i-1], 3)
results["CAP_HEIGHT"] = cap_height

results["SHIELDBOX_HEIGHT"] = round(C_SB_height[4], 3)
results["DATA FILE"] = " "

data["results"] = results

filename3 = 'RAL_LS_database_results.txt'
file3 = open(filename3, "w")
import json
json.dump(data, file3, indent = 2, separators=(", ", ": "))
file3.close()


end = input("press enter to end")