#Emma Buchanan 20/05/2021
#To run: python3 Hybrid_X.py smartscope_filename.txt 
#still to do, include HCC difference from reference and total package height
#seperate code to make common data format

import numpy as np
import matplotlib.pyplot as plt
import sys
import seaborn as sns
import math
import configparser
from datetime import datetime, timezone


filename = sys.argv[1]

ASIC_thickness =0.300


#Hybrid Y ASIC fiducials
ABC_X_ref = [0.875,8.478, 10.539,18.142, 20.203,27.806, 29.876,37.470, 39.531,47.134, 49.195,56.798, 58.859,66.462, 68.523,76.126, 78.187,85.790, 87.851,95.454,]
HCC_X_ref=[6.698,11.629]
ABC_Y_ref=-1.193
HCC_Y_ref=-7.315

plane_count=0

i_DZ_ASIC = list()
i_DZ_package = list()

GP_X = list()
GP_Y = list()
GP_Z = list()

ASIC_X = list()
ASIC_Y = list()
ASIC_Z = list()

jig_X = list()
jig_Y = list()
jig_Z = list()

axis_fid_X = list()
axis_fid_Y = list()
X_measured = list() #x coordinate of ASIC fiducials           
Y_measured = list() #y coordinate of ASIC fiducials
X_diff = list() 
Y_diff = list()

Z_points = list()

cx_count = 0
cy_count = 0
HCC_cx_count = 0
HCC_cy_count = 0

with open(filename) as fn:
    print("opening", filename)
    for line in fn:
        #the z heights for the glue and the total package height 
        if "DZ" in line:
            a=line.split()
            if plane_count <11:
                i_DZ_ASIC.append(((float(a[3]))-ASIC_thickness)*1000)
            else:
                i_DZ_package.append((float(a[3]))*1000)
            plane_count+=1
            #coordinates of Gold pad and ASICs
        for l in range(0,11):
            for i in range(1,5):
                x_name = "GP"+str(l)+"_p"+str(i)+".X"
                y_name = "GP"+str(l)+"_p"+str(i)+".Y"
                z_name = "GP"+str(l)+"_p"+str(i)+".Z"
                if x_name in line:
                    x=line.split()
                    GP_X.append(float(x[3]))
                    
                if y_name in line:
                    y=line.split()
                    GP_Y.append(float(y[3]))
                    
                if z_name in line:
                    z=line.split()
                    GP_Z.append(float(z[3]))
                    
                if l<10:    
                    x_name = 'ABC{}_p{}.X'.format(l, i)
                    y_name = 'ABC{}_p{}.Y'.format(l, i)
                    z_name = 'ABC{}_p{}.Z'.format(l, i)
                    if x_name in line:
                        x=line.split()
                        ASIC_X.append(float(x[3]))
                        
                        
                    if y_name in line:
                        y=line.split()
                        ASIC_Y.append(float(y[3]))
                       
                        
                    if z_name in line:
                        z=line.split()
                        ASIC_Z.append(float(z[3]))
                    
                if l==10:
                    x_name = "HCC_p{}.X".format(i)
                    y_name = "HCC_p{}.Y".format(i)
                    z_name = "HCC_p{}.Z".format(i)
                    if x_name in line:
                        x=line.split()
                        ASIC_X.append(float(x[3]))
                        
                    if y_name in line:
                        y=line.split()
                        ASIC_Y.append(float(y[3]))
                    
                    if z_name in line:
                        z=line.split()
                        ASIC_Z.append(float(z[3]))
                        
        for q in range(1,8):
            x_name = "jig"+str(q)+".X"
            y_name = "jig"+str(q)+".Y"
            z_name = "jig"+str(q)+".Z"

            if x_name in line:
                x=line.split()
                jig_X.append(float(x[3]))
            
            if y_name in line:
                y=line.split()
                jig_Y.append(float(y[3]))
            if z_name in line:
                z=line.split()
                jig_Z.append(float(z[3]))

        #Fiducials        
        name ="X Hybrid_F"
        if name in line:

                x=line.split()
                axis_fid_X.append(float(x[3]))

        name ="Y Hybrid_F"
        if name in line:
                x=line.split()
                axis_fid_Y.append(float(x[3]))
        for i in range(10):
            name = "X ABC"+str(i)+"_fid"
            if name in line:
                x=line.split()
                X_measured.append(float(x[3]))
                X_diff.append((X_measured[cx_count]-ABC_X_ref[cx_count])*1000)
                cx_count += 1
            name = "Y ABC"+str(i)+"_fid"
            if name in line:
                y=line.split()
                Y_measured.append(float(y[3]))
                Y_diff.append((Y_measured[cy_count]-ABC_Y_ref)*1000)
                cy_count +=1
        HCC_nameX =  "X HCC_fid"
        if HCC_nameX in line:
            x=line.split()
            X_measured.append(float(x[3]))
            cx_count += 1
            HCC_cx_count += 1
        HCC_nameY =  "Y HCC_fid"
        if HCC_nameY in line:
            y=line.split()
            Y_measured.append(float(y[3]))
            cy_count += 1
            HCC_cy_count += 1
        for l in range(0, 10):
            for j in range (1,5):
                ASIC_point = 'DZ ABC'+str(l)+'_p' + str(j) + '_tp'

                if ASIC_point in line:
                    a = line.split()
                    Z_points.append(float(a[3]))
                    
            
        for j in range(1,5):
            if 'DZ HCC_p{}_tph'.format(j) in line:
                a = line.split()
                Z_points.append(float(a[3]))



#arranging coordinates into groups of 4
xpos_gp = np.zeros([11,4])
xpos_ASIC = np.zeros([11,4])
ypos_gp = np.zeros([11,4])
ypos_ASIC = np.zeros([11,4])
zpos_gp = np.zeros([11,4])
zpos_ASIC = np.zeros([11,4])
                            
count =0
count2=0
for i in range(len(ASIC_Z)):
    xpos_gp[count2,count]=GP_X[i]
    xpos_ASIC[count2,count]=ASIC_X[i]
    ypos_gp[count2,count]=GP_Y[i]
    ypos_ASIC[count2,count]=ASIC_Y[i]
    zpos_gp[count2,count]=GP_Z[i]
    zpos_ASIC[count2,count]=ASIC_Z[i]
    count+=1
    if count ==4:
        count=0
        count2+=1


#calculating averages to compare with plane z heights
zpos_ASIC_average = list()
zpos_GP_average = list()
diff_glue_height = list()
for i in range(0,11):
    meanA=0
    meangp=0
    for j in range(0,4):
        meanA+=zpos_ASIC[i,j]
        meangp+=zpos_gp[i,j]
        
        if j==3:
            zpos_ASIC_average.append(meanA/4)
            zpos_GP_average.append(meangp/4)
            diff_glue_height.append(((zpos_ASIC_average[i]-zpos_GP_average[i])-ASIC_thickness)*1000)


            
#ASIC Tilt front end and back end HCC
#front end
deltaX_ftilt = np.zeros([10])
deltaY_ftilt = np.zeros([10])
sqrt_ftilt = np.zeros([10])
ftilt = np.zeros([10])

#backend                                                                   
deltaX_btilt = np.zeros([10])
deltaY_btilt = np.zeros([10])
sqrt_btilt = np.zeros([10])
btilt = np.zeros([10])

for i in range(10):
    deltaX_ftilt[i] = (xpos_ASIC[i,3]-xpos_ASIC[i,0]) ** 2
    deltaY_ftilt[i] = (ypos_ASIC[i,3]-ypos_ASIC[i,0]) ** 2
    sqrt_ftilt[i] = math.sqrt(deltaX_ftilt[i]+deltaY_ftilt[i])            \
        
    ftilt[i] = abs(zpos_ASIC[i,3]-zpos_ASIC[i,0])/sqrt_ftilt[i]
    
    deltaX_btilt[i] = (xpos_ASIC[i,2]-xpos_ASIC[i,1]) ** 2
    deltaY_btilt[i] = (ypos_ASIC[i,2]-ypos_ASIC[i,1]) ** 2
    sqrt_btilt[i] = math.sqrt(deltaX_btilt[i]+deltaY_btilt[i])            \
        
    btilt[i] = abs(zpos_ASIC[i,1]-zpos_ASIC[i,0])/sqrt_btilt[i]
    
#HCC tilt X and Y
HCC =list()

i=10 #HCC information is stored here                                       

deltaX_xtilt = (xpos_ASIC[i,3]-xpos_ASIC[i,0]) ** 2
deltaY_xtilt = (ypos_ASIC[i,3]-ypos_ASIC[i,0]) ** 2
sqrt_xtilt = math.sqrt(deltaX_xtilt+deltaY_xtilt)
xtilt = abs(zpos_ASIC[i,3]-zpos_ASIC[i,0])/sqrt_xtilt
HCC.append(xtilt)

deltaX_ytilt = (xpos_ASIC[i,1]-xpos_ASIC[i,0]) ** 2
deltaY_ytilt = (ypos_ASIC[i,1]-ypos_ASIC[i,0]) ** 2
sqrt_ytilt = math.sqrt(deltaX_ytilt+deltaY_ytilt)
ytilt = abs(zpos_ASIC[i,1]-zpos_ASIC[i,0])/sqrt_ytilt
HCC.append(ytilt)

                

#Average package heights 
 
Z_points_array = np.reshape(Z_points, (11, 4))
tph_av = np.average(Z_points_array*1000, axis = 1)






    
axis_labels = ["ABCY0", "ABCY1", "ABCY2", "ABCY3", "ABCY4", "ABCY5", "ABCY6", "ABCY7", "ABCY8", "ABCY9", "HCCY"]


################################################
# Figure with all subplots
#################################################
plt.rcParams.update({'axes.labelsize': 6, 'axes.titlesize':10, 'xtick.labelsize':8, 'ytick.labelsize':8})
fig = plt.figure(figsize = (12, 6))
axes = fig.subplots(nrows = 2, ncols=4)

plot_plotly = False

axis_labels = ["ABCY0", "ABCY1", "ABCY2", "ABCY3", "ABCY4", "ABCY5", "ABCY6", "ABCY7", "ABCY8", "ABCY9", "HCCX"]


#Plot Glue thickness 
spec_max_glue = 160
spec_min_glue = 80

background = 'white'
if min(diff_glue_height)<spec_min_glue: 
    ymin = min(diff_glue_height) - 20
    background = 'lightcoral'
    plot_plotly = True
else:
    ymin = spec_min_glue - 20
if max(diff_glue_height)>spec_max_glue:
    ymax = max(diff_glue_height) + 20
    background = 'lightcoral'
    plot_plotly = True
else:
    ymax = spec_max_glue + 20

Hybrid_DZ=sns.scatterplot(x=axis_labels, y=diff_glue_height,   marker='D', color='#8829F0', ax = axes[0][0])
Hybrid_DZ.set(ylabel='Glue Thickness [\u03BCm]', ylim=(ymin,ymax), title = 'Glue Thickness', facecolor = background)
Hybrid_DZ.set_xticklabels(axis_labels, rotation = 45)
Hybrid_DZ.axes.axhline(y = 80, color='black', linewidth=1, linestyle='--')
Hybrid_DZ.axes.axhline(y = 160, color='black', linewidth=1, linestyle='--')


#Plot Package Height
try:
    spec_max_p = 840
    spec_min_p = 760

    background = 'white'
    if min(tph_av)<spec_min_p: 
        ymin = min(tph_av) - 50
        background = 'lightcoral'
        plot_plotly = True
    else:
        ymin = spec_min_p - 50
    if max(tph_av)>spec_max_p:
        ymax = max(tph_av) + 50
        background = 'lightcoral'
        plot_plotly = True
    else:
        ymax = spec_max_p + 50


    Hybrid_DZ_p=sns.scatterplot(x=axis_labels, y=tph_av,   marker='D', color='#8829F0', ax = axes[0][1])
    Hybrid_DZ_p.set(ylabel='Total Package Height [\u03BCm]', ylim=(ymin, ymax), title = 'Package Height', facecolor = background)
    Hybrid_DZ_p.axes.axhline(y = 760, color='black', linewidth=1, linestyle='--')
    Hybrid_DZ_p.axes.axhline(y = 840, color='black', linewidth=1, linestyle='--')
    Hybrid_DZ_p.set_xticklabels(axis_labels,rotation=45)
except:
    print('missing graph; package heights')

#Plot ASIC positions X
X_diff_plot = np.multiply(X_diff, 0.001)

spec_max_x = 0.1
spec_min_x = -0.1

background = 'white'
if min(X_diff_plot)<spec_min_x: 
    ymin = min(X_diff_plot) - 0.2
    background = 'lightcoral'
    plot_plotly = True
else:
    ymin = spec_min_x - 0.2
if max(X_diff_plot)>spec_max_x:
    ymax = max(X_diff_plot) + 0.2
    background = 'lightcoral'
    plot_plotly = True
else:
    ymax = spec_max_x + 0.2
Asic_x = sns.scatterplot(x = np.arange(0,len(X_diff_plot)), y = X_diff_plot , marker = 'D', color = 'green', ax = axes[0][2])
Asic_x.set(xlabel = 'Measurement Number', ylabel='Difference from reference X coordinate [mm]', ylim=(ymin, ymax), title = 'Asic Positions X', facecolor = background)
Asic_x.axes.axhline(y = -0.1, color='black', linewidth=1, linestyle='--')
Asic_x.axes.axhline(y = 0.1, color='black', linewidth=1, linestyle='--')
Asic_x.set_xticks(np.arange(0,21,1))
Asic_x.set_xticklabels(np.arange(1,22,1), fontsize=6)


#Plot ASIC positions Y
Y_diff_plot = np.multiply(Y_diff, 0.001)

spec_max_y = 0.1
spec_min_y = -0.1

background = 'white'
if min(Y_diff_plot)<spec_min_y: 
    ymin = min(Y_diff_plot) - 0.2
    background = 'lightcoral'
    plot_plotly = True
else:
    ymin = spec_min_y - 0.2
if max(Y_diff_plot)>spec_max_y:
    ymax = max(Y_diff_plot) + 0.2
    background = 'lightcoral'
    plot_plotly = True
else:
    ymax = spec_max_y + 0.2
Asic_y = sns.scatterplot(x = np.arange(0, len(Y_diff_plot)), y = Y_diff_plot, marker = 'D', color = 'yellow', ax = axes[0][3])
Asic_y.set( xlabel = 'Measurement Number', ylabel='Difference from reference Y coordinate [mm]', title = 'Asic Positions Y', facecolor = background)
Asic_y.axes.axhline(y = -0.1, color='black', linewidth=1, linestyle='--')
Asic_y.axes.axhline(y = 0.1, color='black', linewidth=1, linestyle='--')
Asic_y.set_xticks(np.arange(0,21,1))
Asic_y.set_xticklabels(np.arange(1,22,1), fontsize = 6)

#Plot Front-End Tilt for ASICs
axis_labels = ["ABCY0", "ABCY1", "ABCY2", "ABCY3", "ABCY4", "ABCY5", "ABCY6", "ABCY7", "ABCY8", "ABCY9"]

spec_max_tilt = 0.025

background = 'white'
if max(ftilt) > spec_max_tilt:
    background = 'lightcoral'
    plot_plotly = True
    ymax = max(ftilt) + 0.005
else:
    ymax = 0.025
ax_ftilt = sns.scatterplot(x=axis_labels, y=ftilt, ax = axes[1][0], marker = 'D', color = 'red')
ax_ftilt.set(xlabel = 'Measurement Number', ylabel = 'Tilt', ylim=(0, ymax), title = 'Front-End Tilt', facecolor = background);
ax_ftilt.set_xticklabels(axis_labels, rotation = 45);

#Plot Back-End Tilt for ASICS
background = 'white'
if max(btilt) > spec_max_tilt:
    background = 'lightcoral'
    plot_plotly = True
    ymax = max(btilt) + 0.005
else:
    ymax = 0.025
ax_btilt = sns.scatterplot(x=axis_labels, y=btilt, ax = axes[1][1], marker = 'D', color = 'green')
ax_btilt.set(xlabel = 'Measurement Number', ylabel = 'Tilt', ylim=(0, ymax), title = 'Back-End Tilt', facecolor = background);
ax_btilt.set_xticklabels(axis_labels, rotation = 45);

#Plot HCC tilt (X & Y)
tilt_values = [xtilt, ytilt]
if max(tilt_values)>0.025:
    background = 'lightcoral'
    plot_plotly = True
    ymax = max(tilt_values) + 0.005
else:
    background = 'white'
    ymax = 0.025
ax_HCCtilt = sns.scatterplot(x=['x', 'y'], y=tilt_values, ax = axes[1][2], marker = 'D', color = 'orange')
ax_HCCtilt.margins(1,1)
ax_HCCtilt.set(xlabel = None, ylabel = 'Tilt', ylim=(0, ymax), title = 'HCC Tilt',  facecolor = background);

fig.delaxes(axes[1][3])
fig.suptitle('Hybrid Y', fontsize = 20)
plt.tight_layout(rect = [0, 0, 1, 0.97])
plt.show(block = False)

#Ask whether to create plotly graphs

request_plotly = None
if plot_plotly == True:
    request_plotly = input("Plot interactive graphs? (y/n)")




####################################
## Interactive Plots with Plotly
####################################
if request_plotly == "y":
    
    import plotly.graph_objects as go
    from plotly.subplots import make_subplots

    axis_labels = ["ABCX0", "ABCX1", "ABCX2", "ABCX3", "ABCX4", "ABCX5", "ABCX6", "ABCX7", "ABCX8", "ABCX9", "HCCX"]

    fig = make_subplots(
        rows=4, cols=2, subplot_titles=('Glue Thickness', 'Package Height', 'ASIC Position X', 'ASIC Position Y', 'Front-end Tilt', 'Back-end Tilt', 'HCC Tilt'),
        horizontal_spacing=0.2)



    #Glue Heights
    spec_max_glue = 160
    spec_min_glue = 80
    
        


    fig.add_trace(
        go.Scatter(x=axis_labels,
                    y=i_DZ_ASIC,
                    name = 'SmartScope',
                    mode="markers",
                    hoverinfo = 'x+y',
                    showlegend=False,
                    fillcolor = background,
                    marker=dict(color='purple')),
        row=1, col=1
    )


    fig.add_hline(y = spec_max_glue, line_dash = 'dash', line_color = 'black', row = 1, col = 1)
    fig.add_hline(y = spec_min_glue, line_dash = 'dash', line_color = 'black', row = 1, col = 1)
    fig.update_yaxes(title_text = "Glue Thickness [\u03BCm]", row = 1, col = 1)
    fig.update_xaxes(tickangle = 45, row = 1, col = 1)

    #Plot Package Heights
    spec_max_p = 840
    spec_min_p = 760

    try:
        fig.add_trace(
            go.Scatter(x=axis_labels,
                        y=tph_av,
                        mode = 'markers',
                        hoverinfo = 'x+y',
                        showlegend = False,
                        marker = dict(color = 'blue')),
                        row = 1, col = 2
        )

        fig.add_hline(y = spec_max_p, line_dash = 'dash', line_color = 'black', row = 1, col = 2)
        fig.add_hline(y = spec_min_p, line_dash = 'dash', line_color = 'black', row = 1, col = 2)
        fig.update_yaxes(title_text = "Package Height [\u03BCm]", row = 1, col = 2)
        fig.update_xaxes(tickangle = 45, row = 1, col = 2)
    except:
        print('graph missing')

    ##Plot Asic Positions
    #X position

    X_diff_plot = np.multiply(X_diff, 0.001)

    spec_max_x = 0.1
    spec_min_x = -0.1


    fig.add_trace(
        go.Scatter(x = np.arange(0,20,1),
                    y=X_diff_plot,
                    mode = 'markers',
                    hoverinfo = 'x+y',
                    showlegend = False,
                    marker = dict(color = 'green')),
                    row = 2, col = 1
    )

    fig.add_hline(y = spec_max_x, line_dash = 'dash', line_color = 'black', row = 2, col = 1)
    fig.add_hline(y = spec_min_x, line_dash = 'dash', line_color = 'black', row = 2, col = 1)
    fig.update_xaxes(title_text = "Measurement Number", row = 2, col = 1)
    fig.update_yaxes(title_text = "Difference from reference X coordinate [mm]", row = 2, col = 1)

    #Y position
    Y_diff_plot = np.multiply(X_diff, 0.001)


    fig.add_trace(
        go.Scatter(x = np.arange(0,20,1),
                    y=Y_diff_plot,
                    mode = 'markers',
                    hoverinfo = 'x+y',
                    showlegend = False,
                    marker = dict(color = 'yellow')),
                    row = 2, col = 2
    )


    fig.add_hline(y = spec_max_x, line_dash = 'dash', line_color = 'black', row = 2, col = 2)
    fig.add_hline(y = spec_min_x, line_dash = 'dash', line_color = 'black', row = 2, col = 2)
    fig.update_xaxes(title_text = "Measurement Number", row = 2, col = 2)
    fig.update_yaxes(title_text = "Difference from reference Y coordinate [mm]", row = 2, col = 2)


    ##Plot front- and back-end tilt
    spec_max_tilt = 0.025
    #Front

    fig.add_trace(
        go.Scatter(x = np.arange(0,10,1),
                    y=ftilt,
                    mode = 'markers',
                    hoverinfo = 'x+y',
                    showlegend = False,
                    marker = dict(color = 'red')),
                    row = 3, col = 1
    )

    fig.add_hline(y = spec_max_tilt, line_dash = "dash", line_color = 'black', row = 3, col = 'all')
    fig.update_xaxes(title_text = "Measurement Number", row = 3, col = 1)
    fig.update_yaxes(title_text = "Tilt", row = 3, col = 1)

    #Back

    fig.add_trace(
        go.Scatter(x = np.arange(0,10,1),
                    y=btilt, 
                    mode = 'markers',
                    hoverinfo = 'x+y',
                    showlegend = False,
                    marker = dict(color = 'red')),
                    row = 3, col = 2
    )

    fig.add_hline(y = spec_max_tilt, line_dash = 'dash', line_color = 'black', row = 3, col = 2)
    fig.update_xaxes(title_text = "Measurement Number", row = 3, col = 2)
    fig.update_yaxes(title_text = "Tilt", row = 3, col = 2)

    #HCC tilt
    tilt_values = [xtilt, ytilt]

    fig.add_trace(
        go.Scatter(x = ['x', 'y'],
                    y=tilt_values,
                    mode = 'markers',
                    hoverinfo = 'x+y',
                    showlegend = False,
                    marker = dict(color = 'orange')),
                    row = 4, col = 1
    )

    fig.add_hline(y = spec_max_tilt, line_dash = 'dash', line_color = 'black', row = 4, col = 1)
    fig.update_yaxes(title_text = "Tilt", row = 4, col =1)

    fig.update_layout(width = 1000, height = 2000)

    fig.show()





############################################
# Output data to database files
############################################

#set config file
config_file = 'database_metrology.config'
parser = configparser.ConfigParser()
parser.read(config_file)

##Output to file1
#output file for the database                                                 
filename =parser.get('output_file_name','name')
file1 = open(filename,"w")

date = parser.get('header', 'meas_date')
time = parser.get('header', 'meas_time')
#Write header for file1
header = """#---Header:                                                       
EC or Barrel:\t"""+parser.get('header','ec_or_barrel')+"""     
Hybrid Type:\t"""+parser.get('header','hybrid_type')+"""  
Hybrid Ref Number:\t"""+parser.get('header','hybrid_ref')+"""   
Measurement Date/Time:\t"""+datetime.strptime(date + " " + time, "%Y-%m-%d %H:%M:%S").astimezone().isoformat()+","+"""  
Institute:\t"""+parser.get('header','institute')+"""    
Operator:\t"""+parser.get('header','operator')+"""      
Instrument Used:\t"""+parser.get('header','instrument')+"""      
Test Run Number:\t"""+parser.get('header','test_run_num')+"""   
Measurement Program Name:\t"""+parser.get('header','meas_program')+"""   
#---Position Scan:                                                  
#Location    X [mm]    Y [mm]"""

file1.write(header)

#position fiducial information for txt file                          
CHIP_ID =['H_X_P1','H_X_P2','ABC_X_9_P1','ABC_X_9_P2','ABC_X_8_P1','ABC_X_8_P2','ABC_X_7_P1','ABC_X_7_P2','ABC_X_6_P1','ABC_X_6_P2','ABC_X_5_P1','ABC_X_5_P2','ABC_X_4_P1','ABC_X_4_P2','ABC_X_3_P1','ABC_X_3_P2','ABC_X_2_P1','ABC_X_2_P2','ABC_X_1_P1','ABC_X_1_P2','ABC_X_0_P1','ABC_X_0_P2', 'HHC_X_0_p1', 'HHC_X_0_p2']
        
string1='\n'+str(CHIP_ID[0])+'\t'+str(axis_fid_X[0])+'\t'+str(axis_fid_Y[0])#+'\n'
#print(string1)
file1.write(string1)
string1='\n'+str(CHIP_ID[1])+'\t'+str(axis_fid_X[1])+'\t'+str(axis_fid_Y[1])#+'\n'
file1.write(string1)
#print(string1)

X_measured.reverse()
Y_measured.reverse()
for i in range(len(X_measured)):
    X_measured[i] = round(X_measured[i], 3)
    Y_measured[i] = round(Y_measured[i], 3)
for i in range(2,22):
    if(i%2)==0:
        string1='\n'+str(CHIP_ID[i])+'\t'+str(X_measured[i+1])+'\t'+str(Y_measured[i+1])#+'\n'
#        print(string1)
        file1.write(string1)
    else:
        string1='\n'+str(CHIP_ID[i])+'\t'+str(X_measured[i-1])+'\t'+str(Y_measured[i-1])#+'\n'
 #       print(string1)
        file1.write(string1)


string1='\n'+str(CHIP_ID[22])+'\t'+str(X_measured[1])+'\t'+str(Y_measured[1])#+'\n'
#print(string1)
file1.write(string1)

string1='\n'+str(CHIP_ID[23])+'\t'+str(X_measured[0])+'\t'+str(Y_measured[0])#+'\n'
#print(string1)
file1.write(string1)


#height scan                                                                                                   
file1.write('\n#---Height Scan:\n')
file1.write('#Location    Type    X [mm]    Y [mm]     Z [mm]')

CHIP_ID =['ABC_X_9','ABC_X_8','ABC_X_7','ABC_X_6','ABC_X_5','ABC_X_4','ABC_X_3','ABC_X_2','ABC_X_1','ABC_X_0']

CHIP_ID.reverse()
GP_X.reverse()
GP_Y.reverse()
GP_Z.reverse()
ASIC_X.reverse()
ASIC_Y.reverse()
ASIC_Z.reverse()


for i in range(len(GP_X)):
    
    GP_X[i]=round(GP_X[i],2)
    GP_Y[i]=round(GP_Y[i],2)
    GP_Z[i]=round(GP_Z[i],2)
    ASIC_X[i]=round(ASIC_X[i],3)
    ASIC_Y[i]=round(ASIC_Y[i],3)
    ASIC_Z[i]=round(ASIC_Z[i],3)    


s=4
t=4

for i in range(9,-1,-1):
    for j in range(0,8):
        if j==0 or j==4:
            c=0
        if j<4:
            string1 = '\n'+str(CHIP_ID[i])+'\t\t'+str(1)+'\t'+str(GP_X[s])+'\t'+str(GP_Y[s])+'\t'+str(GP_Z[s])
 #           print(string1)
            file1.write(string1)
            c+=1
            s+=1

        if j>3:
            string1 = '\n'+str(CHIP_ID[i])+"\t\t"+str(2)+'\t'+str(ASIC_X[t])+'\t'+str(ASIC_Y[t])+'\t'+str(ASIC_Z[t])
#            print(string1)
            file1.write(string1)
            c+=1
            t+=1


s=0
t=0
CHIP_ID =['HCC_X_0']
for j in range(0,8):
        if j==0 or j==4:
            c=0
        if j<4:
            string1 = '\n'+str(CHIP_ID[0])+'\t\t'+str(1)+'\t'+str(GP_X[s])+'\t'+str(GP_Y[s])+'\t'+str(GP_Z[s])
 #           print(string1)
            file1.write(string1)
            c+=1
            s+=1
        if j>3:
            string1 = '\n'+str(CHIP_ID[0])+"\t\t"+str(2)+'\t'+str(ASIC_X[t])+'\t'+str(ASIC_Y[t])+'\t'+str(ASIC_Z[t])
#            print(string1)
            file1.write(string1)
            c+=1
            t+=1

for i in range(len(jig_X)):
    jig_X[i] = round(jig_X[i], 3)
    jig_Y[i] = round(jig_Y[i], 3)

for j in range(0,7):
    string1 = '\n'+"JIG"+"\t"+"0"+"\t"+str(jig_X[j])+"\t"+str(jig_Y[j])+"\t"+str(jig_Z[j])
    file1.write(string1)

file1.close()



#For file2 - JSON file
filename2 =parser.get('output_file_name2','name')
file2 = open(filename2,"w")

CHIP_ID =['ABC_X_9','ABC_X_8','ABC_X_7','ABC_X_6','ABC_X_5','ABC_X_4','ABC_X_3','ABC_X_2','ABC_X_1','ABC_X_0', 'HCC_X_0']

data = {}
data['component'] = parser.get('header2', 'component')
data['testType'] = parser.get('header2', 'testType')
data['institution'] = parser.get('header2', 'institution')
data['runNumber'] = parser.get('header2', 'runNumber')
data['date'] = parser.get('header2', 'date')
data['passed'] = parser.get('header2', 'passed')
data['problems'] = parser.get('header2', 'problems')

properties = {}

properties['USER'] = parser.get('header2', 'user')
properties['SETUP'] = parser.get('header2', 'setup')
properties['SCRIPT_VERSION'] = parser.get('header2', 'SCRIPT_VERSION')

data['properties'] = properties


results = {}

position = {}
p = 0
for i in range(0,11):
    exec("position[CHIP_ID[{I}]] = [[X_measured[{P}], Y_measured[{P}]], [X_measured[{P2}], Y_measured[{P2}]]]".format(I=i, P=p, P2 = p+1))
    p+=2
results["POSITION"] = position


for i in range(len(diff_glue_height)):
    diff_glue_height[i]=round(diff_glue_height[i],2)

height = {}
for i in range(0, 11):
    exec("height[CHIP_ID[{I}]] = diff_glue_height[{I}]".format(I=i))
results["HEIGHT"] = height

for i in range(len(tph_av)):
    tph_av[i]=round(tph_av[i],2)
    

total_height = {}
for i in range(0,11):
    exec("total_height[CHIP_ID[{I}]] = tph_av[{I}]".format(I = i))
results["TOTAL_HEIGHT"] = total_height

tilt = {}
for i in range(0,10):
    exec("tilt[CHIP_ID[{I}]] = [ftilt[{I}], btilt[{I}]]".format(I = i))
tilt[CHIP_ID[10]] = [HCC[0], HCC[1]]
results["TILT"] = tilt

results["FILE"] = 'null'

data['results'] = results

import json
json.dump(data, file2, indent = 2, separators=(", ", ": "))

file2.close()

finish = input('Press Enter to end')